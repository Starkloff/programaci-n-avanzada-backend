var express = require('express');
var router = express.Router();
var redis = require('redis')
// const bodyParser = require("express");

// router.use(bodyParser.urlencoded ( {extended: true}));

// var clients = [
// {nombre: "Ignacio", nombre: "Pedro"},
// {apellido: "Starkloff", apellido: "Cuerpo"}
// ]

// router.use(function timeLog(req, res, next) {
//   console.log('Time: ', Date.now());
//   next();
// });
router.get('/', function(req,res,next) {
  redis.getAll("clients")
  res.json(
    {status: 200, response: {mensaje: `estas en la carpeta raiz`}}
    );
});
router.get('/clients', (req,res) => {
  redis.get()
  res.json(
    {status: 200, response: {mensaje: "estas leyendo a todos los clientes"}}
    );
});
router.get('/clients/:clienteid', (req,res) => {
  res.json(
    {status: 200 , response: {mensaje:`Estas leyendo al cliente ${req.params.clienteid}`}}
    );
});
router.post('/clients', (req,res) => {
  var cliente = redis.createClient({
    id: req.body.id,
    nombre:req.body.nombre,
    apellido: req.body.apellido
  })
  res.json(
    {status:200, response: {mensaje:`Estas creando al cliente: ${req.body.id}`, datos: `cliente creado: ` + cliente}}
    );
});
router.put('/clients/:clienteid', (req,res) => {
  res.json(
    {status:200, mensaje:`Estas modificando al cliente ${req.params.clienteid} con los datos: ${req.body}`}
    );
});
router.delete('/clients:clienteid', (req,res) => {
  res.json(
    {status: 200, mensaje:`Estas eliminando al cliente ${req.params.clienteid}`});
});

router.get('/clients/:clienteid/contracts', (req,res) => {
  res.json(
    {status: 200, mensaje: `Estas leyendo los contratos del cliente ${req.params.clienteid}`}
    );
});
router.get('/clients/:clienteid/contracts/:contratoid', (req,res) => {
  res.json(
    {status: 200, mensaje:`Estas leyendo el contrato ${req.contratoid} del cliente ${req.params.clienteid}`}
    );
});
router.post('/clients/:clienteid/contracts', (req,res) => {
  res.json(
    {status: 200, mensaje: `Estas creando contratos al cliente ${clienteid} con los datos: ${req.body}`});
});
router.put('/clients/:clienteid/contracts/:contratoid', (req,res) => {
  res.json(
    {status: 200, mensaje: `Estas modificando el contrato ${contratoid} al cliente ${req.params.clienteid} con los datos: ${req.body}`});
});
router.delete('/clients/:clienteid/contracts/:contratoid', (req,res) => {
  res.json(
    {status: 200, mensaje: `Estas eliminando al contrato ${contratoid} del cliente ${req.params.clienteid}`});
});

module.exports = router;
